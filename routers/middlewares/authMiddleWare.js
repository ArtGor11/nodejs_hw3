const jwt = require('jsonwebtoken');
const config = require('../../config/default.json');
const {secret} = config;

const authMiddleWare = async (req, res, next) => {
  const header = req.header('Authorization');

  if (!header) {
    return res.status(400)
        .json({message: `No Authorization http header found!`});
  }

  const [tokenType, token] = header.split(' ');

  console.log('Token type: ', tokenType);

  if (tokenType !== 'JWT') {
    return res.status(400).json({message: 'No JWT prefix!'});
  }

  if (!token) {
    return res.status(400).json({message: 'No token found!'});
  }

  try {
    req.user = jwt.verify(token, secret);
    next();
  } catch (error) {
    return res.status(400).json({message: 'Bad request'});
  }
};

module.exports = {authMiddleWare};
