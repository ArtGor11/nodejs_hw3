const express = require('express');
const router = new express.Router();
const { authMiddleWare } = require('./middlewares/authMiddleWare');
const { Truck } = require('../models/Truck');

router.post('/', authMiddleWare, async (req, res) => {
  if (req.user.role === 'DRIVER') {
    const { id } = req.user;
    const { type } = req.body;
    try {
      const truck = new Truck({ created_by: id, type });
      await truck.save();
      res.status(200).json({ message: 'Truck added successfully' });
    } catch (err) {
      console.log(err);
      res.status(400).json({ message: err.message });
    }
  } else {
    return res.status(400).json({ message: 'Available only for DRIVER role' });
  }
});

router.get('/', authMiddleWare, async (req, res) => {
  const { id } = req.user;
  try {
    const trucks = await Truck.find({ created_by: id });
    res.status(200).json({trucks: trucks});
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
});

router.get('/:id', authMiddleWare, async (req, res) => {
  try {
    const truck = await Truck.findById(req.params.id);

    if (!truck) {
      throw ({ status: 400, message: 'No truck with given id found' });
    };

    res.status(200).json(truck);
  } catch (err) {
    res.status(400).json({ message: err.message });
    console.log(err);
  }
});

router.put('/:id', authMiddleWare, async (req, res) => {
  try {
    await Truck.findByIdAndUpdate(req.params.id, req.body);
    res.status(200).json({ message: 'Truck details changed successfully' });
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
});

router.post('/:id/assign', authMiddleWare, async (req, res) => {
  const { _id } = req.user;
  try {
    const assignedTruck = await Truck.findOne({ created_by: _id });
    if (assignedTruck) {
      if (assignedTruck.status === 'OL') {
        throw { status: 404, message: 'You can\'t assign truck while on load' };
      }

      assignedTruck.assigned_to = null;
      assignedTruck.status = null;
      await assignedTruck.save();
    }
    await Truck.findByIdAndUpdate(req.params.id, { assigned_to: _id, status: 'IS' });
    res.status(200).json({ message: 'Truck assigned successfully' });
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
});

router.delete('/:id', authMiddleWare, async (req, res) => {
  try {
    await Truck.findByIdAndDelete(req.params.id);
    res.status(200).json({ message: 'Truck deleted successfully' });
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
});

module.exports = router;
