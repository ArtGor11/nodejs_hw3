const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const nodemailer = require('nodemailer');

const { secret } = require('../config/default.json');
const { User } = require('../models/User');

const register = async (req, res) => {
  const { email, password, role } = req.body;
  try {
    const user = new User({
      email,
      password: await bcrypt.hash(password, 10),
      role,
    });

    await user.save();
    res.json({ message: 'User created successfully' });
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
};

const login = async (req, res) => {
  const { email, password } = req.body;

  const user = await User.findOne({ email });

  if (!user) {
    return res
      .status(400)
      .json({ message: `${email} email not found!` });
  }

  if (!(await bcrypt.compare(password, user.password))) {
    return res.status(400).json({ message: 'Wrong password!' });
  }

  const token = jwt.sign(
    { id: user.id, email: user.email, role: user.role, created_date: user.created_date },
    secret,
  );
  res.status(200).json({ message: 'Success', jwt_token: token });
};

const forgotPassword = async (req, res) => {
  const { email } = req.body;

  const user = await User.findOne({ email });

  if (!user) {
    return res
      .status(400)
      .json({ message: `${email} not found!` });
  }

  // async..await is not allowed in global scope, must use a wrapper
  async function main() {
    // Generate test SMTP service account from ethereal.email
    // Only needed if you don't have a real mail account for testing
    const testAccount = await nodemailer.createTestAccount();

    // create reusable transporter object using the default SMTP transport
    const transporter = nodemailer.createTransport({
      host: 'smtp.ethereal.email',
      port: 587,
      secure: false, // true for 465, false for other ports
      auth: {
        user: testAccount.user, // generated ethereal user
        pass: testAccount.pass, // generated ethereal password
      },
    });

    // send mail with defined transport object
    const info = await transporter.sendMail({
      from: '"Fred Foo 👻" <foo@example.com>', // sender address
      to: `${email}, `, // list of receivers
      subject: 'Forgot password', // Subject line
      text: `Your new password : `, // plain text body
      html: `<b>Forgot password?</b><br><p>Your new password : </p>`, // html body
    });

    console.log('Message sent: %s', info.messageId);
    // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>

    // Preview only available when sending through an Ethereal account
    console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));
    // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...
  }

  main().catch(console.error);
  res.status(200).json({ message: 'New password sent to your email address' });
};


module.exports = { register, login, forgotPassword };
