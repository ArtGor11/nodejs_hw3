const Load = require('../models/Load');
const Truck = require('../models/Truck');
const config = require('../config/default.json');
const { loadStates, truckTypes } = config;

const addUserLoad = async (req, res) => {
  if (req.user.role === 'SHIPPER') {
    const { name, payload, pickup_address, delivery_address, dimensions } = req.body;
    const id = req.user.id;
    const load = new Load({ created_by: id, assigned_to: id, name, payload, pickup_address, delivery_address, dimensions });
    try {
      await load.save();
      res.status(200).json({ message: 'Load created successfully' });
    } catch (err) {
      res.status(400).json({ message: err.message });
    }
  } else {
    return res.status(400).json({ message: 'abailable only for SHIPPER role' });
  }
};

const getLoad = async (req, res) => {
  const { id } = req.user;
  try {
    const loads = await Load.find({ $or: [{ created_by: id }, { assigned_to: id }] });
    res.json({ loads: loads });

  } catch (err) {
    res.status(400).json({ message: err.message });
  }
};

const getLoadById = async (req, res) => {
  try {
    const load = await Load.findOne({ id: req.params.id, status: { $in: ['NEW', 'POSTED'] } });
    res.status(200).json({ load: load });
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
};

const deleteLoadById = async (req, res) => {
  try {
    await Load.findByIdAndDelete(req.params.id);
    res.status(200).json({ message: 'Load deleted successfully' });
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
};

const getActiveLoads = async (req, res) => {
  const { id, role } = req.user;
  try {
    if (role === 'DRIVER') {
      const loads = await Load.find({ assigned_to: id }, { status: 'ASSIGNED' });
      res.json(loads);
    } else {
      throw { status: 403, message: 'Forbidden' };
    }
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
};

const iterateNextLoad = async (req, res) => {
  try {
    const load = await Load.findOne({ assigned_to: req.user.id, status: 'ASSIGNED' });
    const truck = await Truck.findOne({ assigned_to: req.user.id, status: 'OL' });
    if (!load) {
      throw { status: 400, message: 'You have no active loads at this moment' };
    }
    const currentIndex = loadStates.indexOf(load.state);
    if (load.state === loadStates[loadStates.length - 2]) {
      load.state = loadStates[loadStates.currentIndex + 1];
      load.status = 'SHIPPED';
      load.logs.push(`Load state changed to ${loadStates[currentIndex + 1]} by driver ${load.assigned_to}`);
      load.logs.push(`Load status changed to SHIPPED by driver ${load.assigned_to}`);
      truck.status = 'IS';
    } else {
      load.state = loadStates[currentIndex + 1];
      load.logs.push(`Load state changed to ${loadStates[currentIndex + 1]} by driver ${load.assigned_to}`);
    }

    await load.save();
    await truck.save();

    res.status(200).json(load);
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
};

const updateLoadById = async (req, res) => {
  try {
    if (req.user.role !== 'SHIPPER') {
      throw { status: 403, message: 'Forbidden' };
    }
    const update = await Load.findByIdAndUpdate(req.params.id, req.body);
    res.status(200).json({ message: "Load details changed successfully" });
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
};

const postLoadById = async (req, res) => {
  if (req.user.role !== 'SHIPPER') {
    throw { status: 403, message: 'Forbidden' };
  }
  try {
    const load = await Load.findById(req.params.id);
    load.status = 'POSTED';
    await load.save();
    const compatibleType = await truckTypes.find((el) => {
      return el.max_weight >= load.payload &&
      el.width >= load.dimensions.width &&
      el.height >= load.dimensions.height &&
      el.length >= load.dimensions.length;
    });  
    const availableTrucks = await Truck.find({assigned_to: {$ne: null}}, {status: 'IS'}, {type: compatibleType.type});    
    
    if (availableTrucks.length<1) {
      throw {status: 200, message: 'No suitable truck found'};
    }

    const chosenTruck = availableTrucks[0];
    const chosenDriver = chosenTruck.assigned_to;

    chosenTruck.status = 'OL';

    load.assigned_to = chosenDriver;
    load.status = 'ASSIGNED';
    load.state = 'En route to Pick Up';
    load.logs.push({message: `Load assigned to driver with id ${chosenDriver}`});
    await load.save();
    await chosenTruck.save();

    res.status(200).json({message: 'Load posted successfully', driver_found: true});
  } catch (err) {
    res.status(400).json({ message: err.message });
  }


};

const getLoadInfoById = async (req, res) => {
  try {
    if (req.user.role !== 'SHIPPER') {
      throw { status: 403, message: 'Forbidden' };
    }

    const load = await Load.findOne({ id: req.params.id, status: 'ASSIGNED' });
    const truck = await Truck.findOne({ assigned_to: load.assigned_to });
    res.status(200).json({ load, truck });
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
};

module.exports = {
  getLoad,
  addUserLoad,
  getActiveLoads,
  iterateNextLoad,
  getLoadById,
  updateLoadById,
  deleteLoadById,
  postLoadById,
  getLoadInfoById
};
