const mongoose = require('mongoose');
const { Schema } = mongoose;

const truckSchema = new Schema({
  created_by: {
    ref: 'user',
    type: Schema.Types.ObjectId,
  },
  assigned_to: {
    type: String,
    default: null,
  },
  type: {
    type: String,
    enum: ['SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT'],
    required: true,
  },
  status: {
    type: String,
    enum: [null, 'OL', 'IS'],
    default: 'IS',
  },
  created_date: {
    type: Date,
    default: Date.now(),
  },
},
  { versionKey: false },
);

module.exports.Truck = mongoose.model('Truck', truckSchema);
